#include<string>
#include<iostream>
#include<random>
using namespace std;

string randDNA(int seed,string bases, int n){
	
	mt19937 eng1(seed);
	int min =0; //minimum number to generate
	int max =3; //maximum number to generate
	uniform_int_distribution<> uniform(min, max);//random
	int index =0;
	string dna =" ";
	
	char DnaBases[4] = {'A', 'C', 'T', 'G'}; //Create an array
	
	for(int i=0; i<n; i++)
	{
		index =uniform(eng1);
		dna = dna + DnaBases[index];
		cout << index;
	}//adds A C T G to the output string
	
	return dna;//returns the output string
}
